from django.conf.urls import url
from .views import index, add_message
from .views import index, add_message, message_table

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_message', add_message, name='add_message'),
    url(r'^result_table', message_table, name='result_table'),
    ]